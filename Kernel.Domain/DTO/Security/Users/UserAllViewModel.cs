﻿using Kernel.Entities.Security;
using System;
using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Users
{
    public class UserAllViewModel
    {
        [Required]
        public int id_user { get; set; }

        [Required]
        public string username { get; set; }

        [Required]
        public DateTime created_at { get; set; }

        public string company_name { get; set; }
    }
}
