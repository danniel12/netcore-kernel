﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Companies
{
    public class CreateViewModel
    {
        [Required]
        public string company_name { get; set; }
    }
}
