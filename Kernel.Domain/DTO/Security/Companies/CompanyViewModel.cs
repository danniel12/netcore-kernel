﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Companies
{
    public class CompanyViewModel
    {
        [Required]
        public int id_company { get; set; }

        [Required]
        public string company_name { get; set; }

        [Required]
        public DateTime created_at { get; set; }
    }
}
