﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Companies
{
    public class UpdateViewModel
    {
        [Required]
        public string id_company { get; set; }
    }
}
