﻿using Kernel.Data;
using Kernel.Domain.DTO.Security.Companies;
using Kernel.Domain.Exceptions;
using Kernel.Domain.Interfaces;
using Kernel.Entities.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Domain.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly DbContextKernel _context;
        private readonly IConfiguration _config;

        public CompanyService(DbContextKernel context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public async Task Create(CreateViewModel model)
        {
            var company_name = model.company_name.ToLower();

            if (await _context.Companies.AnyAsync(u => u.company_name == company_name))
                throw new KernelConflictException();

            try
            {
                Company company = new Company
                {
                    company_name = company_name
                };

                _context.Companies.Add(company);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<CompanyViewModel>> List()
        {
            var companies = _context.Companies.Select(u => new CompanyViewModel()
            {
                id_company = u.id_company,
                company_name = u.company_name,
                created_at = u.created_at
            }).ToListAsync();

            return await companies;
        }

        //public async Task Update(UpdateViewModel model)
        //{
        //    var id_company = model.id_company;

        //    if (await _context.Companies.AnyAsync(u => u.company_name == company_name))
        //        throw new KernelConflictException();

        //    try
        //    {
        //        Company company = new Company
        //        {
        //            company_name = company_name
        //        };

        //        _context.Companies.Add(company);
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
