﻿using Kernel.Data;
using Kernel.Domain.DTO.Security.Users;
using Kernel.Domain.Exceptions;
using Kernel.Domain.Interfaces;
using Kernel.Entities.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly DbContextKernel _context;
        private readonly IConfiguration _config;

        public UserService(DbContextKernel context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public async Task Create(CreateViewModel model)
        {
            var username = model.username.ToLower();

            if (await _context.Users.AnyAsync(u => u.username == username))
                throw new KernelConflictException();

            try
            {
                CreatePasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);

                User user = new User
                {
                    is_admin = false,
                    username = username,
                    password_hash = passwordHash,
                    password_salt = passwordSalt,
                };

                _context.Users.Add(user);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task ChangePassword(ChangePasswordViewModel model)
        {
            var username = model.username.ToLower();

            var user = await _context.Users.Where(c => c.username == username).FirstOrDefaultAsync();

            if (user == null)
                throw new KernelAuthenticationException();

            if (!CheckPasswordHash(model.old_password, user.password_hash, user.password_salt))
                throw new KernelAuthenticationException();

            try
            {
                CreatePasswordHash(model.new_password, out byte[] passwordHash, out byte[] passwordSalt);

                user.password_hash = passwordHash;
                user.password_salt = passwordSalt;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<LoginResultModel> Login(LoginViewModel model)
        {
            var username = model.username.ToLower();

            var user = await _context.Users.Where(u => u.username == username)
                .FirstOrDefaultAsync();


            if (user == null)
                throw new KernelAuthenticationException();

            if (!CheckPasswordHash(model.password, user.password_hash, user.password_salt))
                throw new KernelAuthenticationException();

            return new LoginResultModel()
            {
                token = GetToken(user, username)
            };


        }

        public async Task<LoginResultModel> Refresh(HttpRequest Request)
        {
            StringValues authzHeaders;
            Request.Headers.TryGetValue("Authorization", out authzHeaders);
            var bearerToken = authzHeaders.ElementAt(0);
            string token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;

            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = true,
                ValidIssuer = _config["Jwt:Issuer"],
                ValidAudience = _config["Jwt:Issuer"],
                ClockSkew = TimeSpan.FromSeconds(10),
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]))
            };

            SecurityToken validatedToken;
            ClaimsPrincipal principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
            if (validatedToken.ValidTo.AddMinutes(10) >= DateTime.Now)
            {
                var user = await _context.Users.Where(u => u.username == principal.Identity.Name).FirstOrDefaultAsync();

                if (user == null)
                    throw new KernelAuthenticationException();

                return new LoginResultModel()
                {
                    token = GetToken(user, user.username)
                };
            }
            else throw new KernelAuthenticationException();
        }

        public async Task<List<UserViewModel>> List()
        {
            return await _context.Users.Where(u => u.is_admin == false).Select(u => new UserViewModel()
            {
                id_user = u.id_user,
                username = u.username,
            }).ToListAsync();
        }

        public async Task Update(UpdateViewModel model)
        {
            var user = await _context.Users.Where(u => u.id_user == model.id_user).FirstOrDefaultAsync();

            if (user == null) throw new KernelNotFoundException();

            await _context.SaveChangesAsync();
        }

        public async Task UpdatePassword(UpdatePasswordViewModel model)
        {
            var user = await _context.Users.Where(u => u.id_user == model.id_user).FirstOrDefaultAsync();

            if (user == null) throw new KernelNotFoundException();

            CreatePasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);

            user.password_hash = passwordHash;
            user.password_salt = passwordSalt;

            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id_user)
        {
            var user = await _context.Users.Where(u => u.id_user == id_user && u.is_admin == false).FirstOrDefaultAsync();

            if (user == null) throw new KernelNotFoundException();

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        private string MakeToken(List<Claim> claims)
        {

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds,
                claims: claims);

            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private string GetToken(User user, string username)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.id_user.ToString()),
                new Claim(ClaimTypes.Name,user.username),
                new Claim(ClaimTypes.Role, "User"),
                new Claim("Name", user.username),
                new Claim("Admin", user.is_admin ? "1": "0"),
            };
            if (user.is_admin) claims.Add(new Claim(ClaimTypes.Role, "Admin"));

            return MakeToken(claims);
        }

        private bool CheckPasswordHash(string password, byte[] passwordHashStored, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                return new ReadOnlySpan<byte>(passwordHashStored).SequenceEqual(new ReadOnlySpan<byte>(passwordHash));
            }
        }

        public async Task<List<UserAllViewModel>> ListAllUsers()
        {
            var users = _context.Users.Include("Company").ToList();

            var usuarios = _context.Users.Include("Company").Select(u => new UserAllViewModel()
            {
                id_user = u.id_user,
                username = u.username,
                created_at = u.created_at,
                company_name = u.Company.company_name
            }).ToListAsync();

            return await usuarios;
        }
    }
}
