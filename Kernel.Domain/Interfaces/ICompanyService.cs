﻿using Kernel.Domain.DTO.Security.Companies;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kernel.Domain.Interfaces
{
    public interface ICompanyService
    {
        Task Create(CreateViewModel model);
        Task<List<CompanyViewModel>> List();

        //Task Update(UpdateViewModel model);
    }
}
