﻿using Kernel.Domain.DTO.Security.Users;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kernel.Domain.Interfaces
{
    public interface IUserService
    {
        Task ChangePassword(ChangePasswordViewModel model);
        Task Create(CreateViewModel model);
        Task<LoginResultModel> Login(LoginViewModel model);
        Task<LoginResultModel> Refresh(HttpRequest Request);
        Task<List<UserViewModel>> List();
        Task Update(UpdateViewModel model);
        Task UpdatePassword(UpdatePasswordViewModel model);
        Task Delete(int id_user);
        Task<List<UserAllViewModel>> ListAllUsers();
    }
}