﻿using System;

namespace Kernel.Domain.Exceptions
{
    public class KernelForbiddenException : KernelException
    {
        public KernelForbiddenException() { }

        public KernelForbiddenException(Exception innerException) : base(innerException) { }

        public KernelForbiddenException(string MSG) : base(MSG) { }

        public KernelForbiddenException(Exception innerException, string MSG) : base(innerException, MSG) { }
    }
}
