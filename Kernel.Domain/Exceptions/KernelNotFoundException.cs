using System;

namespace Kernel.Domain.Exceptions
{
    public class KernelNotFoundException : KernelException
    {
        public KernelNotFoundException() { }

        public KernelNotFoundException(Exception innerException) : base(innerException) { }

        public KernelNotFoundException(string MSG) : base(MSG) { }

        public KernelNotFoundException(Exception innerException, string MSG) : base(innerException, MSG) { }
    }
}