using System;

namespace Kernel.Domain.Exceptions
{
    public class KernelConflictException : KernelException
    {
        public KernelConflictException() : base() { }
        public KernelConflictException(Exception innerException) : base(innerException) { }
        public KernelConflictException(string MSG) : base(MSG) { }
        public KernelConflictException(Exception innerException, string MSG) : base(innerException, MSG) { }
    }
}