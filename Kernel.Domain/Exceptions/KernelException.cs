﻿namespace Kernel.Domain.Exceptions
{
    public class KernelException : System.Exception
    {
        public static readonly string IDENTIFICADOR = "[ QUIZ EXCEPTION ]";
        public static readonly string DEFAULT = "An error has occurred";
        public KernelException() : base($"{IDENTIFICADOR} : {DEFAULT}") { }
        public KernelException(string MSG) : base($"{IDENTIFICADOR} : {MSG}") { }
        public KernelException(System.Exception innerException) : base($"{IDENTIFICADOR} : {DEFAULT}", innerException) { }
        public KernelException(System.Exception innerException, string MSG) : base($"{IDENTIFICADOR} : {MSG}", innerException) { }
    }
}
