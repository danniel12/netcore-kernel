﻿using System;
using System.Linq;
using System.Security.Claims;

namespace Kernel.API.Auxiliars
{
    public class Token
    {
        public static int GetIdUser(ClaimsPrincipal User)
        {
            int id_user = 0;
            Int32.TryParse(User.Claims.Where(u => u.Type == ClaimTypes.NameIdentifier).Select(u => u.Value).FirstOrDefault(), out id_user);
            return id_user;

        }
    }
}
