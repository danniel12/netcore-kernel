﻿using Kernel.API.DTO;
using Kernel.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;

namespace Kernel.API.Filters
{
    public class GlobalExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private ILogger<GlobalExceptionFilterAttribute> logger;
        public GlobalExceptionFilterAttribute(ILogger<GlobalExceptionFilterAttribute> logger)
        {
            this.logger = logger;
        }
        public override void OnException(ExceptionContext context)
        {
            context.HttpContext.Response.ContentType = "application/json";

            var TrackId = Guid.NewGuid();
            var exception = context.Exception;

            if (exception is KernelAuthenticationException)
            {
                context.HttpContext.Response.StatusCode = 403;
                context.Result = new JsonResult(new KernelResultError()
                {
                    TrackId = TrackId,
                    Code = 403,
                    Msg = exception.Message
                });
            }

            else if (exception is KernelConflictException)
            {
                context.HttpContext.Response.StatusCode = 409;
                context.Result = new JsonResult(new KernelResultError()
                {
                    TrackId = TrackId,
                    Code = 409,
                    Msg = exception.Message
                });
            }

            else if (exception is KernelForbiddenException)
            {
                context.HttpContext.Response.StatusCode = 403;
                context.Result = new JsonResult(new KernelResultError()
                {
                    TrackId = TrackId,
                    Code = 403,
                    Msg = exception.Message
                });
            }

            else if (exception is KernelNotFoundException)
            {
                context.HttpContext.Response.StatusCode = 404;
                context.Result = new JsonResult(new KernelResultError()
                {
                    TrackId = TrackId,
                    Code = 404,
                    Msg = exception.Message
                });
            }
            else
            {
                logger.LogCritical(new EventId(), exception, $"TRACKID: {TrackId.ToString()}");

                context.HttpContext.Response.StatusCode = 500;
                context.Result = new JsonResult(new KernelResultError()
                {
                    TrackId = TrackId,
                    Code = 500,
                    Msg = "An unexpected error has occurred. Please contact us."
                });
            }
        }
    }
}