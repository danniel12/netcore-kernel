﻿using System;

namespace Kernel.API.DTO
{
    public class KernelResultError
    {
        public Guid TrackId { get; set; }
        public int Code { get; set; }
        public string Msg { get; set; }
    }
}
