﻿using Kernel.Domain.DTO.Security.Companies;
using Kernel.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Kernel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompaniesController(ICompanyService companyService)
        {
            _companyService = companyService;
        }
        // GET: api/<CompaniesController>
        

        [HttpPost("[action]")]
        public async Task<IActionResult> Create([FromBody] CreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _companyService.Create(model);

            return Ok("Se creo correctamente");
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<CompanyViewModel>>> List()
        {
            return (await _companyService.List());
        }
    }
}
