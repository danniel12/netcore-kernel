﻿using Kernel.Domain.DTO.Security.Users;
using Kernel.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kernel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([FromBody] CreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _userService.Create(model);

            return Ok();
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _userService.ChangePassword(model);

            return Ok();

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(await _userService.Login(model));
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> Refresh()
        {
            return Ok(await _userService.Refresh(Request));
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<List<UserViewModel>>> List()
        {
            return (await _userService.List());
        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update([FromBody] UpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _userService.Update(model);
            return Ok();
        }

        [HttpPut("[action]")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdatePassword([FromBody] UpdatePasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _userService.UpdatePassword(model);
            return Ok();
        }

        [HttpDelete("{id_user}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete([FromRoute] int id_user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _userService.Delete(id_user);
            return Ok();
        }

        [HttpGet("[action]")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<List<UserAllViewModel>>> ListAllUsers()
        {
            return await _userService.ListAllUsers();
        }

        [HttpGet("/products3")]
        public string prueba1()
        {
            return "Hola";
        }
    }
}
