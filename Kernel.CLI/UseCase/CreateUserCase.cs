﻿using Kernel.Data;
using Kernel.Domain.Interfaces;
using Kernel.Domain.Services;
using Kernel.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Kernel.CLI.UseCase
{
    public class CreateUserCase
    {
        private DbContextKernel _context;
        private IConfiguration _config;
        private IUserService _userService;

        public CreateUserCase(DbContextKernel context, IConfiguration config)
        {
            _context = context;
            _config = config;
            _userService = new UserService(context, config);
        }

        public async Task Execute()
        {
            var validUsername = false;
            string username, password;

            do
            {
                Console.Write("Username: ");

                username = Console.ReadLine().ToLower();
                if (await _context.Users.Where(u => u.username == username).AnyAsync())
                {
                    Console.WriteLine();
                    Console.WriteLine("Exist another User with the same Username");
                }
                else validUsername = true;
            } while (!validUsername);

            var validPassword = false;
            do
            {
                Console.WriteLine();
                Console.Write("Password: ");
                password = Console.ReadLine();
                if (password.Length < 8)
                {
                    Console.WriteLine("The Password must be have a least 8 Characters");
                }
                else validPassword = false;
            } while (validPassword);

            var user = new User()
            {
                is_admin = true,
                username = username,
                password_hash = new byte[0],
                password_salt = new byte[0]
            };

            _context.Add(user);
            await _context.SaveChangesAsync();

            await _userService.UpdatePassword(new Domain.DTO.Security.Users.UpdatePasswordViewModel()
            {
                id_user = user.id_user,
                password = password
            });

            Console.WriteLine();
            Console.WriteLine("The User has been created successfully");
            Console.ReadLine();
        }

    }
}
