﻿using Kernel.Data;
using Kernel.Domain.Interfaces;
using Kernel.Domain.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Kernel.CLI.UseCase
{
    public class ChangeUserPasswordCase
    {
        private DbContextKernel _context;
        private IConfiguration _config;
        private IUserService _userService;

        public ChangeUserPasswordCase(DbContextKernel context, IConfiguration config)
        {
            _context = context;
            _config = config;
            _userService = new UserService(context, config);
        }

        public async Task Execute()
        {
            var validUsername = false;
            string username, password;

            do
            {
                Console.Write("Username: ");

                username = Console.ReadLine().ToLower();
                if (!(await _context.Users.Where(u => u.username == username).AnyAsync()))
                {
                    Console.WriteLine();
                    Console.WriteLine("No exist a User this Username");
                }
                else validUsername = true;
            } while (!validUsername);

            var validPassword = false;
            do
            {
                Console.WriteLine();
                Console.Write("Password: ");
                password = Console.ReadLine();
                if (password.Length < 8)
                {
                    Console.WriteLine("The Password must be have a least 8 Characters");
                }
                else validPassword = false;
            } while (validPassword);

            var user = await _context.Users.Where(u => u.username == username).FirstOrDefaultAsync();

            await _userService.UpdatePassword(new Domain.DTO.Security.Users.UpdatePasswordViewModel()
            {
                id_user = user.id_user,
                password = password
            });

            Console.WriteLine();
            Console.WriteLine("The Password has been changed successfully");
            Console.ReadLine();
        }

    }
}
