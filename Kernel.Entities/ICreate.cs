﻿using System;

namespace Kernel.Entities
{
    public interface ICreate
    {
        public DateTime created_at { get; set; }
    }
}
