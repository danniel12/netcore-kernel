﻿using System;

namespace Kernel.Entities
{
    public interface IUpdate
    {
        public DateTime updated_at { get; set; }
    }
}
