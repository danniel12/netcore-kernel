﻿using System;

namespace Kernel.Entities
{
    public interface ISoftDelete
    {
        public DateTime? deleted_at { get; set; }
    }
}
