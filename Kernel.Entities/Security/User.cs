﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kernel.Entities.Security
{
    public class User : ICreate, IUpdate, ISoftDelete
    {
        [Required]
        public int id_user { get; set; }

        [Required]
        public bool is_admin { get; set; }

        [Required]
        public string username { get; set; }

        [Required]
        public byte[] password_hash { get; set; }

        [Required]
        public byte[] password_salt { get; set; }

        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime? deleted_at { get; set; }

        [ForeignKey("id_company")]
        public Company Company { get; set; }
    }
}
