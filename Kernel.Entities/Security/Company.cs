﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kernel.Entities.Security
{
    public class Company : ICreate, IUpdate, ISoftDelete
    {
        [Required]
        public int id_company { get; set; }

        [Required]
        public string company_name { get; set; }

        [Required]
        public bool custom_place { get; set; }

        [Required]
        public string custom_place_type { get; set; }

        [Required]
        public bool other_branches_adversaries { get; set; }

        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime? deleted_at { get; set; }

        //public virtual ICollection<User> User { set; get; }
    }
}
