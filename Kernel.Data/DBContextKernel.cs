﻿using Audit.EntityFramework;
using Kernel.Data.Mapping.Security;
using Kernel.Entities;
using Kernel.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Kernel.Data
{
    public sealed class DbContextKernel : DbContext
    {
        private static DbContextHelper _helper = new DbContextHelper();
        private readonly IAuditDbContext _auditContext;

        public DbContextKernel(DbContextOptions<DbContextKernel> options) : base(options)
        {
            _auditContext = new DefaultAuditContext(this);
            _helper.SetConfig(_auditContext);

        }

        #region Security
        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        #endregion

        public override int SaveChanges()
        {
            CreateTimeStamp();
            UpdateTimeStamp();
            UpdateSoftDeleteStatuses();

            //return base.SaveChanges();
            return _helper.SaveChanges(_auditContext, () => base.SaveChanges());
        }



        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            CreateTimeStamp();
            UpdateTimeStamp();
            UpdateSoftDeleteStatuses();

            //return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            return await _helper.SaveChangesAsync(_auditContext, () => base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken));
        }

        private void UpdateSoftDeleteStatuses()
        {
            ChangeTracker.DetectChanges();

            var markedAsDeleted = ChangeTracker.Entries().Where(x => x.State == EntityState.Deleted);

            foreach (var item in markedAsDeleted)
            {
                if (item.Entity is ISoftDelete entity)
                {
                    item.State = EntityState.Unchanged;
                    entity.deleted_at = DateTime.Now;
                }
            }


            var markedAsInsert = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);

            foreach (var item in markedAsInsert)
            {
                if (item.Entity is ISoftDelete entity)
                {
                    entity.deleted_at = null;
                }
            }
        }

        private void UpdateTimeStamp()
        {
            var updatedItems = ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);

            foreach (var item in updatedItems)
            {
                if (item.Entity is IUpdate entity)
                {
                    entity.updated_at = DateTime.Now;
                }
            }
        }

        private void CreateTimeStamp()
        {
            var createdItems = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);

            foreach (var item in createdItems)
            {
                if (item.Entity is ICreate entity)
                {
                    entity.created_at = DateTime.Now;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Security
            modelBuilder.ApplyConfiguration(new CompanyMap());
            modelBuilder.ApplyConfiguration(new UserMap());
            #endregion

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                if (typeof(ISoftDelete).IsAssignableFrom(entityType.ClrType))
                {
                    entityType.AddSoftDeleteQueryFilter();
                }
            }
        }
    }

    public static class SoftDeleteQueryExtension
    {
        public static void AddSoftDeleteQueryFilter(
            this IMutableEntityType entityData)
        {
            var methodToCall = typeof(SoftDeleteQueryExtension)
                .GetMethod(nameof(GetSoftDeleteFilter),
                    BindingFlags.NonPublic | BindingFlags.Static)
                .MakeGenericMethod(entityData.ClrType);
            var filter = methodToCall.Invoke(null, new object[] { });
            entityData.SetQueryFilter((LambdaExpression)filter);
        }

        private static LambdaExpression GetSoftDeleteFilter<TEntity>()
            where TEntity : class, ISoftDelete

        {
            Expression<Func<TEntity, bool>> filter = x => x.deleted_at == null;
            return filter;
        }
    }
}


