-- Table: users
CREATE TABLE users (
    id_user int NOT NULL AUTO_INCREMENT,
    is_admin tinyint NOT NULL DEFAULT ((0)),
    id_company int NULL,
    username varchar(100) NOT NULL,
    password_hash longblob NOT NULL,
    password_salt longblob NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    deleted_at datetime NULL,
    CONSTRAINT users_pk PRIMARY KEY (id_user),
    INDEX users_idx_1 (is_admin ASC),
    INDEX users_idx_2 (id_company ASC),
    INDEX users_idx_3 (username ASC),
    INDEX users_idx_4 (created_at ASC),
    INDEX users_idx_5 (updated_at ASC),
    INDEX users_idx_6 (deleted_at ASC)
);

-- Table : companies
CREATE TABLE companies (
    id_company int NOT NULL AUTO_INCREMENT,
	id_google_places_type int NULL,
	custom_place tinyint NOT NULL DEFAULT (0),
	custom_place_type varchar(255) NULL,
    other_branches_adversaries tinyint NOT NULL DEFAULT (0),
	company_name varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    deleted_at datetime NULL,
    CONSTRAINT companies_pk PRIMARY KEY (id_company),
    INDEX companies_idx_1 (id_google_places_type ASC),
	INDEX companies_idx_2 (custom_place ASC),
	INDEX companies_idx_3 (other_branches_adversaries ASC),
	INDEX companies_idx_4 (created_at ASC),
    INDEX companies_idx_5 (updated_at ASC),
    INDEX companies_idx_6 (deleted_at ASC)
);

-- Table : form1
CREATE TABLE form1 (
    id_form1 int NOT NULL AUTO_INCREMENT,
    id_company int NOT NULL,
    id_user int NOT NULL,
    lat decimal(20,17) NOT NULL,
    lon decimal(20,17) NOT NULL,
    branch_code varchar(255) NOT NULL,
    branch_name varchar(1024) NOT NULL,
    income decimal(20,2) NOT NULL,
    month tinyint NOT NULL,
    year smallint NOT NULL,
	address text NULL,
	rating decimal(4,2) NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    deleted_at datetime NULL,
    CONSTRAINT form1_pk PRIMARY KEY (id_form1),
    INDEX form1_idx_1 (id_company ASC),
    INDEX form1_idx_2 (id_user ASC),
    INDEX form1_idx_3 (branch_code ASC),
    INDEX form1_idx_4 (month ASC),
    INDEX form1_idx_5 (year ASC),
    INDEX form1_idx_6 (created_at ASC),
    INDEX form1_idx_7 (updated_at ASC),
    INDEX form1_idx_8 (deleted_at ASC)    
);

-- Table : google_places_types
CREATE TABLE google_places_types (
    id_google_places_type int NOT NULL AUTO_INCREMENT,
    type_code varchar(25) NOT NULL,
    type_name varchar(1024) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    deleted_at datetime NULL,
    CONSTRAINT google_places_types_pk PRIMARY KEY (id_google_places_type),
    INDEX google_places_types_idx_1 (type_code ASC),
    INDEX google_places_types_idx_2 (created_at ASC),
    INDEX google_places_types_idx_3 (updated_at ASC),
    INDEX google_places_types_idx_4 (deleted_at ASC)
);

-- Table : adversaries
CREATE TABLE adversaries (
	id_adversary int NOT NULL AUTO_INCREMENT,
	id_form1 int NOT NULL,
	adversary_name varchar(1024) NOT NULL,
	lat decimal(20,17) NOT NULL,
    lon decimal(20,17) NOT NULL,
	address text NOT NULL,
	rating decimal(4,2) NOT NULL,
	created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    deleted_at datetime NULL,
    CONSTRAINT adversaries_pk PRIMARY KEY (id_adversary),
    INDEX adversaries_idx_1 (id_form1 ASC),
    INDEX adversaries_idx_2 (created_at ASC),
    INDEX adversaries_idx_3 (updated_at ASC),
    INDEX adversaries_idx_4 (deleted_at ASC)
);

-- Foreign Keys

-- Table : users
ALTER TABLE users ADD CONSTRAINT users_companies FOREIGN KEY users (id_company) REFERENCES companies (id_company);

-- Table : form1
ALTER TABLE form1 ADD CONSTRAINT form1_companies FOREIGN KEY form1 (id_company) REFERENCES companies (id_company);
ALTER TABLE form1 ADD CONSTRAINT form1_users FOREIGN KEY form1 (id_user) REFERENCES users (id_user);

-- Table : companies
ALTER TABLE companies ADD CONSTRAINT companies_google_places_types FOREIGN KEY companies (id_google_places_type) REFERENCES google_places_types (id_google_places_type);

-- Table : companies
ALTER TABLE adversaries ADD CONSTRAINT adversaries_form1 FOREIGN KEY adversaries (id_form1) REFERENCES form1 (id_form1);

INSERT INTO `google_places_types` (`id_google_places_type`, `type_code`, `type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'lawyer', 'Abogado', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(2, 'aquarium', 'Acuario', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(3, 'airport', 'Aeropuerto', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(4, 'insurance_agency', 'Agencia de Seguros', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(5, 'travel_agency', 'Agencia de Viajes', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(6, 'real_estate_agency', 'Agencia Inmobiliaria', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(7, 'storage', 'Almacenamiento', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(8, 'lodging', 'Alojamiento', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(9, 'car_rental', 'Alquiler de Coches', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(10, 'movie_rental', 'Alquiler de Pel�culas', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(11, 'tourist_attraction', 'Atracci�n Tur�stica', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(12, 'city_hall', 'Ayuntamiento', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(13, 'bank', 'Banco', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(14, 'bar', 'Bar', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(15, 'library', 'Biblioteca', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(16, 'bowling_alley', 'Boliche', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(17, 'cafe', 'Cafeter�a', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(18, 'atm', 'Cajero Autom�tico', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(19, 'casino', 'Casino', '2020-12-13 18:16:44', '2020-12-13 18:16:44', NULL),
	(20, 'cemetery', 'Cementerio', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(21, 'shopping_mall', 'Centro Comercial', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(22, 'locksmith', 'Cerrajero', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(23, 'movie_theater', 'Cine', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(24, 'night_club', 'Club Nocturno', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(25, 'school', 'Colegio', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(26, 'meal_takeaway', 'Comida para Llevar', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(27, 'accounting', 'Contabilidad', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(28, 'roofing_contractor', 'Contratista de Techos', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(29, 'hair_care', 'Cuidado del Cabello', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(30, 'dentist', 'Dentista', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(31, 'doctor', 'Doctor', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(32, 'drugstore', 'Drogueria', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(33, 'electrician', 'Electricista', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(34, 'embassy', 'Embajada', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(35, 'meal_delivery', 'Entrega de Comida', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(36, 'primary_school', 'Escuela Primaria', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(37, 'secondary_school', 'Escuela Secundaria', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(38, 'bus_station', 'Estaci�n de Autobuses', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(39, 'fire_station', 'Estaci�n de Bomberos', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(40, 'subway_station', 'Estaci�n de Metro', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(41, 'transit_station', 'Estaci�n de Tr�nsito', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(42, 'train_station', 'Estaci�n de Tren', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(43, 'light_rail_station', 'Estaci�n de Tren Ligero', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(44, 'parking', 'Estacionamiento', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(45, 'stadium', 'Estadio', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(46, 'pharmacy', 'Farmacia', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(47, 'hardware_store', 'Ferreter�a', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(48, 'physiotherapist', 'Fisioterapeuta', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(49, 'florist', 'Florer�a', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(50, 'plumber', 'Fontanero', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(51, 'funeral_home', 'Funeraria', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(52, 'art_gallery', 'Galer�a de Arte', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(53, 'gas_station', 'Gasolinera', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(54, 'gym', 'Gimnasio', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(55, 'hospital', 'Hospital', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(56, 'church', 'Iglesia', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(57, 'jewelry_store', 'Joyer�a', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(58, 'courthouse', 'Juzgados', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(59, 'car_wash', 'Lavado de Autos', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(60, 'laundry', 'Lavander�a', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(61, 'book_store', 'Librer�a', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(62, 'mosque', 'Mezquita', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(63, 'moving_company', 'Mudanzas', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(64, 'furniture_store', 'Muebler�a', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(65, 'museum', 'Museo', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(66, 'local_government_office', 'Oficina de Gobierno', '2020-12-13 18:16:45', '2020-12-13 18:16:45', NULL),
	(67, 'post_office', 'Oficina Postal', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(68, 'bakery', 'Panader�a', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(69, 'taxi_stand', 'Parada de Taxi', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(70, 'park', 'Parque', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(71, 'amusement_park', 'Parque de Atracciones', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(72, 'rv_park', 'Parque de Caravanas', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(73, 'painter', 'Pintor', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(74, 'police', 'Polic�a', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(75, 'car_repair', 'Reparaci�n de Autos', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(76, 'restaurant', 'Restaurante', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(77, 'beauty_salon', 'Sal�n de Belleza', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(78, 'synagogue', 'Sinagoga', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(79, 'spa', 'SPA', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(80, 'supermarket', 'Supermercado', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(81, 'hindu_temple', 'Templo Hind�', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(82, 'campground', 'Terreno de Camping', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(83, 'store', 'Tienda', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(84, 'home_goods_store', 'Tienda de Art�culos para el Hogar', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(85, 'bicycle_store', 'Tienda de Bicicletas', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(86, 'convenience_store', 'Tienda de Conveniencia', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(87, 'electronics_store', 'Tienda de Electr�nicos', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(88, 'liquor_store', 'Tienda de Licores', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(89, 'pet_store', 'Tienda de Mascotas', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(90, 'clothing_store', 'Tienda de Ropa', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(91, 'department_store', 'Tiendas Departamentales', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(92, 'university', 'Universidad', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(93, 'car_dealer', 'Vendedor de Autos', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(94, 'veterinary_care', 'Veterinario', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(95, 'shoe_store', 'Zapater�a', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL),
	(96, 'zoo', 'Zool�gico', '2020-12-13 18:16:46', '2020-12-13 18:16:46', NULL);